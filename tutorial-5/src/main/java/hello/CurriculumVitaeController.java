package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CurriculumVitaeController {

    @GetMapping("/curriculum-vitae")
    public String visitor(@RequestParam(name = "visitor", required = false)
                                       String visitor, Model model) {
        model.addAttribute("visitor", visitor);
        return "curriculum-vitae";
    }

}
