package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me! (DONE)

        /* Say good bye to old dated simple for to loop through array of Objects!
        for (int i = 0; i < commands.size(); i++) {
            Command command = commands.get(i);
            command.execute();
        }
        */

        /* Enhanced for loop FTW!
        for (Command command: commands) {
            command.execute();
        }
        */

        // Stream API and Functional Programming is more FTW tho!
        commands.stream().forEach(Command::execute);
    }

    @Override
    public void undo() {
        // TODO Complete me! (DONE)

        /*
        // Classical implementation to undo from last index
        for (int i = commands.size() - 1; i >= 0; i--) {
            Command command = commands.get(i);
            command.undo();
        }
        */

        // Stream API and functional programming is more FTW!
        // Note: This implementation requires additional Memory Complexity due to the copying to LinkedList
        commands.stream().collect(Collectors.toCollection(LinkedList::new)).descendingIterator().forEachRemaining(Command::undo);
    }
}
