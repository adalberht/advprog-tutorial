package id.ac.ui.cs.advprog.tutorial2.exercise1.invoker;

import id.ac.ui.cs.advprog.tutorial2.exercise1.command.Command;
import id.ac.ui.cs.advprog.tutorial2.exercise1.command.NoCommand;

public class RemoteControlWithUndo {

    public static final int NUM_OF_BUTTONS = 7;
    private static final String STR_FMT = "[slot %d] %s    %s\n";

    // Custom Exceptions / Errors Messages
    public static final String SLOT_OUT_OF_BOUND_EXCEPTION_MESSAGE = "There is no button with that slot number.";
    public static final String NULL_UNDO_COMMAND_EXECEPTION_MESSAGE = "You haven't executed any command yet!";


    private Command[] onCommands;
    private Command[] offCommands;
    private Command undoCommand;

    public RemoteControlWithUndo() {
        onCommands = new Command[NUM_OF_BUTTONS];
        offCommands = new Command[NUM_OF_BUTTONS];
        Command noCmd = new NoCommand();

        for (int i = 0; i < NUM_OF_BUTTONS; i++) {
            setCommand(i, noCmd, noCmd);
        }

        undoCommand = noCmd;
    }

    public void setCommand(int slot, Command onCmd, Command offCmd) {
        onCommands[slot] = onCmd;
        offCommands[slot] = offCmd;
    }

    public boolean isValidCommandSlot(int slot) {
        return slot >= 0 && slot < NUM_OF_BUTTONS;
    }

    public void executeCommand(Command command) {
        command.execute();
        undoCommand = command;
    }

    public void onButtonWasPushed(int slot) {
        // TODO Complete me! (DONE)
        if (!isValidCommandSlot(slot)) throw new IndexOutOfBoundsException(SLOT_OUT_OF_BOUND_EXCEPTION_MESSAGE);
        executeCommand(onCommands[slot]);
    }

    public void offButtonWasPushed(int slot) {
        // TODO Complete me! (DONE)
        if (!isValidCommandSlot(slot)) throw new IndexOutOfBoundsException(SLOT_OUT_OF_BOUND_EXCEPTION_MESSAGE);
        executeCommand(offCommands[slot]);
    }

    public void undoButtonWasPushed() {
        // TODO Complete me! (DONE)
        if (undoCommand == null) throw new NullPointerException(NULL_UNDO_COMMAND_EXECEPTION_MESSAGE);
        undoCommand.undo();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("\n------ Remote Control ------\n");
        for (int slot = 0; slot < NUM_OF_BUTTONS; slot++) {
            String entry = String.format(STR_FMT, slot,
                    onCommands[slot].getClass().getName(),
                    offCommands[slot].getClass().getName());
            builder.append(entry);
        }
        builder.append("[undo] " + undoCommand.getClass().getName());

        return builder.toString();
    }
}
