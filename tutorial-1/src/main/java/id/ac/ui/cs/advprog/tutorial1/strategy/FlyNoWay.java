package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        // do-nothing, can't fly
    }
}
