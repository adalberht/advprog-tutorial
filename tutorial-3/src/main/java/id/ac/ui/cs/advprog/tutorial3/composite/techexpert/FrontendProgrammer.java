package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public static final String ROLE = "Front End Programmer";
    public static final double BASE_SALARY = 30000.00;

    // TODO Implement (DONE)
    public FrontendProgrammer(String name, double salary) {
        if (salary < BASE_SALARY) throw new IllegalArgumentException();
        this.name = name;
        this.salary = salary;
        role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
