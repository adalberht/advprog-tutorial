package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
        Company company = new Company();

        List<Supplier<Employees>> employeesCandidates = Arrays.asList(
            () -> new Ceo("Luffy", 500000.00),
            () -> new Cto("Zorro", 320000.00),
            () -> new BackendProgrammer("Franky", 94000.00),
            () -> new BackendProgrammer("Usopp", 200000.00),
            () -> new FrontendProgrammer("Nami", 66000.00),
            () -> new FrontendProgrammer("Robin", 130000.00),
            () -> new  UiUxDesigner("Sanji", 177000.00),
            () -> new NetworkExpert("Brook", 83000.00),
            () -> new SecurityExpert("Chopper", 10000.00)
        );

        for (Supplier<Employees> employeesCandidate: employeesCandidates) {
            try {
                Employees employees = employeesCandidate.get();
                company.addEmployee(employees);
                System.out.printf("Employee with name %s and role %s with salary %f has been added to the company!\n",
                        employees.getName(),
                        employees.getRole(),
                        employees.getSalary());
            } catch (IllegalArgumentException error) {
                System.out.println("The given salary must be greater than the role base salary!");
            }
        }

    }
}
