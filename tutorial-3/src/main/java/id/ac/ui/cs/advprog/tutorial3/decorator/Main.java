package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Buying every type of Breads...");
        Arrays.stream(BreadProducer.values()).forEach(breadProducer -> {
            Food food = breadProducer.createBreadToBeFilled();
            System.out.printf("Adding %s with cost: %s\n", food.getDescription(), food.cost());

            System.out.printf("Buying every combination of Fillings to %s\n", food.getDescription());
            for(int i = 0; i < FillingDecorator.values().length; ++i) {
                food = breadProducer.createBreadToBeFilled();
                for (int j = i; j < FillingDecorator.values().length; ++j) {
                    FillingDecorator fillingDecorator = FillingDecorator.values()[j];
                    food = fillingDecorator.addFillingToBread(food);
                    System.out.printf("Adding %s with cost: %s\n", food.getDescription(), food.cost());
                }
                System.out.println();
            }
            System.out.println();
        });
    }

}
