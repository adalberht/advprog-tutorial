package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThinBunBurger extends Food {
    public ThinBunBurger() {
        // TODO Implement (DONE)
        description = "Thin Bun Burger";
    }

    @Override
    public double cost() {
        // TODO Implement (DONE)
        return 1.5;
    }
}
