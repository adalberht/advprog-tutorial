package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public static final String ROLE = "Network Expert";
    public static final double BASE_SALARY = 50000.00;

    // TODO Implement (DONE)
    public NetworkExpert(String name, double salary) {
        if (salary < BASE_SALARY) throw new IllegalArgumentException();
        this.name = name;
        this.salary = salary;
        role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
