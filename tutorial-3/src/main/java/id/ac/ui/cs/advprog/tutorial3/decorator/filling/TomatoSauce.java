package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Food {
    Food food;

    public TomatoSauce(Food food) {
        // TODO Implement (DONE)
        this.food = food;
    }

    @Override
    public String getDescription() {
        // TODO Implement (DONE)
        return food.getDescription() + ", adding tomato sauce";
    }

    @Override
    public double cost() {
        // TODO Implement (DONE)
        return food.cost() + 0.2;
    }
}
