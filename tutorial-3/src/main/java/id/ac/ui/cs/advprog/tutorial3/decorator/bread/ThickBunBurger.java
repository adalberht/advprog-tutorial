package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    public ThickBunBurger() {
        // TODO Implement (DONE)
        description = "Thick Bun Burger";
    }

    @Override
    public double cost() {
        // TODO Implement (DONE)
        return 2.5;
    }
}
