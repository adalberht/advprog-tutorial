package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class BurntClams implements Clams {

    public String toString() {
        return "Burnt Clams from Thousand Island";
    }
}
