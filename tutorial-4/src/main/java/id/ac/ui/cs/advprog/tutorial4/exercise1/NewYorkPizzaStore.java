package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

public class NewYorkPizzaStore extends PizzaStore {
    static final String FRANCHISE = "New York Style";

    public NewYorkPizzaStore() {
        setFranchise(FRANCHISE);
    }

    @Override
    protected Pizza createPizza(String item) {
        PizzaIngredientFactory ingredientFactory =
                new NewYorkPizzaIngredientFactory();

        Pizza pizza = null;
        switch (item.toLowerCase()) {
            case ("cheese"): {
                pizza = new CheesePizza(ingredientFactory);
                break;
            }
            case ("veggie"): {
                pizza = new VeggiePizza(ingredientFactory);
                break;
            }
            case ("clam"): {
                pizza = new ClamPizza(ingredientFactory);
                break;
            }
            default:
                break;
        }

        if (pizza != null) {
            pizza.setName(getFranchise() + " " + pizza.getName());
        }

        return pizza;
    }
}
