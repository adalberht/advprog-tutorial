package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

public class DepokPizzaStore extends PizzaStore {
    static final String FRANCHISE = "Depok Style";

    public DepokPizzaStore() {
        setFranchise(FRANCHISE);
    }

    @Override
    protected Pizza createPizza(String item) {
        PizzaIngredientFactory ingredientFactory =
                new DepokPizzaIngredientFactory();

        Pizza pizza = null;
        switch (item.toLowerCase()) {
            case ("cheese"): {
                pizza = new CheesePizza(ingredientFactory);
                break;
            }
            case ("veggie"): {
                pizza = new VeggiePizza(ingredientFactory);
                break;
            }
            case ("clam"): {
                pizza = new ClamPizza(ingredientFactory);
                break;
            }
            default:
                break;
        }

        if (pizza != null) {
            pizza.setName(getFranchise() + " " + pizza.getName());
        }

        return pizza;
    }
}
