package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class HugeCrustDough implements Dough {
    public String toString() {
        return "Huge Crust Dough";
    }
}
