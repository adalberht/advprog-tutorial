package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;


public class DepokPizzaStoreTest {
    DepokPizzaStore depokPizzaStore;
    NewYorkPizzaStore newYorkPizzaStore;

    public boolean isVeggiesEquals(Veggies[] veggies1, Veggies[] veggies2) {
        if (veggies1.length != veggies2.length) {
            return false;
        }
        int count = 0;
        for (int i = 0; i < veggies1.length; ++i) {
            Veggies veggie1 = veggies1[i];
            Veggies veggie2 = veggies2[i];
            if (veggie1.toString().equals(veggie2.toString())) {
                ++count;
            }
        }
        return count == veggies1.length;
    }

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        Pizza pizza = depokPizzaStore.createPizza("");
        assertNull(pizza);
    }

    @Test
    public void testCreateCheesePizza() {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(), depokPizzaStore.getFranchise() + " " + CheesePizza.NAME);
    }

    @Test
    public void testCreateCheesePizzaReturnsInstanceOfCheesePizza() {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertTrue(pizza.getClass().isAssignableFrom(CheesePizza.class));
    }

    @Test
    public void testCreateClamPizza() {
        Pizza pizza = depokPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), depokPizzaStore.getFranchise() + " " + ClamPizza.NAME);
    }

    @Test
    public void testCreateClamPizzaReturnsInstanceOfClamPizza() {
        Pizza pizza = depokPizzaStore.createPizza("clam");
        assertTrue(pizza.getClass().isAssignableFrom(ClamPizza.class));
    }

    @Test
    public void testCreateVeggiesPizza() {
        Pizza pizza = depokPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(), depokPizzaStore.getFranchise() + " " + VeggiePizza.NAME);
    }

    @Test
    public void testCreateVeggiesPizzaReturnsInstanceOfVeggiesPizza() {
        Pizza pizza = depokPizzaStore.createPizza("veggie");
        assertTrue(pizza.getClass().isAssignableFrom(VeggiePizza.class));
    }

    @Test
    public void testDepokCheesePizzaShouldNotUseTheSameMaterialAsNewYorkCheesePizza() {
        Pizza depokPizza = depokPizzaStore.createPizza("cheese");
        Pizza newYorkPizza = newYorkPizzaStore.createPizza("cheese");
        depokPizza.prepare();
        newYorkPizza.prepare();
        assertNotEquals(depokPizza.getDough().toString(), newYorkPizza.getDough().toString());
        assertNotEquals(depokPizza.getSauce().toString(), newYorkPizza.getSauce().toString());
        assertNotEquals(depokPizza.getCheese().toString(), newYorkPizza.getCheese().toString());
    }


    @Test
    public void testDepokClamsPizzaShouldNotUseTheSameMaterialAsNewYorkClamsPizza() {
        Pizza depokPizza = depokPizzaStore.createPizza("clam");
        Pizza newYorkPizza = newYorkPizzaStore.createPizza("clam");
        depokPizza.prepare();
        newYorkPizza.prepare();
        assertNotEquals(depokPizza.getDough().toString(), newYorkPizza.getDough().toString());
        assertNotEquals(depokPizza.getSauce().toString(), newYorkPizza.getSauce().toString());
        assertNotEquals(depokPizza.getCheese().toString(), newYorkPizza.getCheese().toString());
        assertNotEquals(depokPizza.getClam().toString(), newYorkPizza.getClam().toString());
    }


    @Test
    public void testDepokVeggiePizzaShouldNotUseTheSameMaterialAsNewYorkVeggiePizza() {
        Pizza depokPizza = depokPizzaStore.createPizza("veggie");
        Pizza newYorkPizza = newYorkPizzaStore.createPizza("veggie");
        depokPizza.prepare();
        newYorkPizza.prepare();
        assertNotEquals(depokPizza.getDough().toString(), newYorkPizza.getDough().toString());
        assertNotEquals(depokPizza.getSauce().toString(), newYorkPizza.getSauce().toString());
        assertNotEquals(depokPizza.getCheese().toString(), newYorkPizza.getCheese().toString());
        assertFalse(isVeggiesEquals(depokPizza.getVeggies(), newYorkPizza.getVeggies()));
    }

}
