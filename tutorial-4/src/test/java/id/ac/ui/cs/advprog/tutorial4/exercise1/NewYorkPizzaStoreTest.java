package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("");
        assertNull(pizza);
    }

    @Test
    public void testCreateCheesePizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(), newYorkPizzaStore.getFranchise() + " " + CheesePizza.NAME);
    }

    @Test
    public void testCreateCheesePizzaReturnsInstanceOfCheesePizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("cheese");
        assertTrue(pizza.getClass().isAssignableFrom(CheesePizza.class));
    }

    @Test
    public void testCreateClamPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), newYorkPizzaStore.getFranchise() + " " + ClamPizza.NAME);
    }

    @Test
    public void testCreateClamPizzaReturnsInstanceOfClamPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("clam");
        assertTrue(pizza.getClass().isAssignableFrom(ClamPizza.class));
    }

    @Test
    public void testCreateVeggiesPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(), newYorkPizzaStore.getFranchise() + " " + VeggiePizza.NAME);
    }

    @Test
    public void testCreateVeggiesPizzaReturnsInstanceOfVeggiesPizza() {
        Pizza pizza = newYorkPizzaStore.createPizza("veggie");
        assertTrue(pizza.getClass().isAssignableFrom(VeggiePizza.class));
    }

}
